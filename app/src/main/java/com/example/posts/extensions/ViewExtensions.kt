package com.example.posts.extensions

import android.view.View


fun View.identify() {
    id = View.generateViewId()
}

var View.isVisible: Boolean
    get() = visibility == View.VISIBLE
    set(value) {
        visibility = if (value) View.VISIBLE else View.GONE
    }