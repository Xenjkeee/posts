package com.example.posts.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.posts.ui.activity.MainActivityUI
import org.jetbrains.anko.AnkoContext

class MainActivity : AppCompatActivity() {

    private var ui: MainActivityUI? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui = MainActivityUI()
        ui?.createView(AnkoContext.create(this, this, true))
        supportActionBar?.elevation = 0f
    }

}