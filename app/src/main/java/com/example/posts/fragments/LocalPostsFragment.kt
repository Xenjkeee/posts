package com.example.posts.fragments

import android.widget.ImageButton
import android.widget.PopupMenu
import com.example.posts.R
import com.example.posts.data.entities.PostEntity
import com.example.posts.data.sources.DataBaseWrapper
import com.example.posts.ui.fragment.EditPostDialogUI
import com.example.posts.ui.fragment.LocalPostsFragmentUI
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.support.v4.alert

class LocalPostsFragment : BasePostsFragment<PostEntity>() {
    override val ui = LocalPostsFragmentUI(this)
    override val hasActions: Boolean = true

    override fun loadData() {
        ui.onDataLoaded(DataBaseWrapper.getAllPosts(PostEntity::class.java))
    }

    override fun itemActionClicked(post: Int, actionButton: ImageButton) {
        PopupMenu(context, actionButton).apply {
            inflate(R.menu.post_action_menu)
            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.edit_post -> showEditDialog(posts[post])
                    R.id.remove_post -> removePost(posts[post])
                }
                return@setOnMenuItemClickListener true
            }
        }.show()
    }

    fun showEditDialog(post: PostEntity? = null) {
        val editPostDialogUI = EditPostDialogUI(post?.title, post?.body)
        alert {

            titleResource = if (post == null) R.string.create_post else R.string.edit_post

            positiveButton(android.R.string.ok) {
                if (post == null) {
                    addPost(editPostDialogUI.titleValue.text.toString(),
                            editPostDialogUI.bodyValue.text.toString())
                } else {
                    editPost(post, editPostDialogUI.titleValue.text.toString(), editPostDialogUI.bodyValue.text.toString())
                }
            }

            negativeButton(android.R.string.cancel) {}

            customView = editPostDialogUI.createView(AnkoContext.create(context!!))
        }.show()
    }

    private fun addPost(title: String, body: String) {
        posts.add(DataBaseWrapper.putPostMyPost(title, body))
        ui.postsAdapter?.notifyItemInserted(posts.size - 1)
    }

    private fun editPost(post: PostEntity, title: String, body: String) {
        ui.postsAdapter?.notifyItemChanged(posts.indexOf(post), true)
        DataBaseWrapper.editPost(post, title, body)
    }

    private fun removePost(post: PostEntity) {
        val postPosition = posts.indexOf(post)
        ui.postsAdapter?.apply {
            notifyItemRemoved(postPosition)
            notifyItemRangeChanged(postPosition, owner.posts.size)
        }
        posts.remove(post)
        DataBaseWrapper.removePost(post)
    }
}
