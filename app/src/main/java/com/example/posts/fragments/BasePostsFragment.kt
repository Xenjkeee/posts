package com.example.posts.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import com.example.posts.data.entities.BasePostModel
import com.example.posts.ui.fragment.BasePostsFragmentUI
import org.jetbrains.anko.AnkoContext

abstract class BasePostsFragment<T : BasePostModel> : Fragment() {

    abstract val hasActions: Boolean
    lateinit var posts: ArrayList<T>

    protected abstract val ui: BasePostsFragmentUI<*>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return ui.createView(AnkoContext.create(context!!))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        loadData()
    }

    open fun itemActionClicked(post: Int, actionButton: ImageButton) {}

    protected abstract fun loadData()
}