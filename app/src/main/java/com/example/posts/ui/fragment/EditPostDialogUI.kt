package com.example.posts.ui.fragment

import android.content.Context
import android.widget.EditText
import com.example.posts.R
import org.jetbrains.anko.*

class EditPostDialogUI(private val title: String? = null, private val body: String? = null) : AnkoComponent<Context> {
    lateinit var titleValue: EditText
    lateinit var bodyValue: EditText

    override fun createView(ui: AnkoContext<Context>) = with(ui) {
        verticalLayout {
            padding = dip(16)
            titleValue = editText(title) {
                hintResource = R.string.post_title
            }
            bodyValue = editText(body) {
                hintResource = R.string.post_body
            }
        }
    }
}