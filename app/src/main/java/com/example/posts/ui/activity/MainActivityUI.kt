package com.example.posts.ui.activity

import android.support.design.widget.TabLayout
import android.support.v4.content.ContextCompat
import com.example.posts.R
import com.example.posts.activities.MainActivity
import com.example.posts.adapters.MainPagerAdapter
import com.example.posts.extensions.identify
import com.example.posts.fragments.InternetPostsFragment
import com.example.posts.fragments.LocalPostsFragment
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.viewPager

class MainActivityUI : AnkoComponent<MainActivity> {
    override fun createView(ui: AnkoContext<MainActivity>) = with(ui) {
        verticalLayout {
            lparams(width = matchParent, height = matchParent)
            val tabs = include<TabLayout>(R.layout.themed_tab_layout) {
                identify()
                setTabTextColors(
                        ContextCompat.getColor(ctx, R.color.light_white),
                        ContextCompat.getColor(ctx, R.color.white)

                )
                elevation = dip(4).toFloat()
            }

            val pager = viewPager {
                identify()
                adapter = MainPagerAdapter(owner.supportFragmentManager,
                        arrayListOf(owner.getString(R.string.my_posts), owner.getString(R.string.internet_posts)),
                        arrayListOf(LocalPostsFragment(), InternetPostsFragment()))

            }.lparams {
                width = matchParent
                height = matchParent
            }
            tabs.setupWithViewPager(pager)

        }
    }
}