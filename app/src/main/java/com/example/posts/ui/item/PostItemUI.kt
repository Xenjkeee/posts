package com.example.posts.ui.item

import android.content.Context
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import com.example.posts.R
import com.example.posts.extensions.isVisible
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView

class PostItemUI(private val hasPopup: Boolean) : AnkoComponent<Context> {
    override fun createView(ui: AnkoContext<Context>) = with(ui) {
        frameLayout {
            lparams(width = matchParent, height = wrapContent)

            cardView {
                id = R.id.post_root
                relativeLayout {

                    padding = dip(16)


                    val actions = imageButton(R.drawable.more_icon) {
                        id = R.id.post_actions
                        backgroundColorResource = android.R.color.transparent
                        isVisible = hasPopup
                    }.lparams {
                        alignParentRight()
                        centerVertically()
                    }


                    // post title
                    val title = textView {
                        id = R.id.post_title
                        textColor = ContextCompat.getColor(ctx, R.color.dark_gray)
                        textSize = 16f
                    }.lparams {
                        alignParentTop()
                        alignParentLeft()
                        leftOf(actions)
                    }

                    //post body
                    textView {
                        id = R.id.post_body
                        textSize = 14f
                        maxLines = 2
                        ellipsize = TextUtils.TruncateAt.END
                    }.lparams {
                        below(title)
                        alignParentLeft()
                        leftOf(actions)
                    }
                }

            }.lparams {
                width = matchParent
                height = wrapContent
                margin = dip(5)
            }
        }
    }
}