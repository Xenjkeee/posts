package com.example.posts.data.entities

interface BasePostModel {

    companion object {
        const val ID = "id"
    }

    var title: String?
    var body: String?
}