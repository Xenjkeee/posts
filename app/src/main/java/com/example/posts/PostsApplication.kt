package com.example.posts

import android.app.Application
import io.realm.Realm

class PostsApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
    }
}