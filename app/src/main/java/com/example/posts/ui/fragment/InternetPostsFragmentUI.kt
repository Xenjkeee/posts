package com.example.posts.ui.fragment

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.ProgressBar
import com.example.posts.data.entities.InternetPostEntity
import com.example.posts.fragments.InternetPostsFragment
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import kotlin.properties.Delegates

class InternetPostsFragmentUI(internetPostsFragment: InternetPostsFragment) : BasePostsFragmentUI<InternetPostEntity>(internetPostsFragment) {
    override var progress: ProgressBar by Delegates.notNull()

    override var recycler: RecyclerView by Delegates.notNull()

    override val hasPopup: Boolean = false

    override fun createView(ui: AnkoContext<Context>) = with(ui) {
        relativeLayout {

            recycler = recyclerView {
                layoutManager = LinearLayoutManager(owner)
                clipToPadding = false
                topPadding = dip(5)
            }

            progress = progressBar().lparams {
                centerInParent()
            }
        }

    }

}


