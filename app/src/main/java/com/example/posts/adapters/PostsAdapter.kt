package com.example.posts.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.example.posts.R
import com.example.posts.data.entities.BasePostModel
import com.example.posts.extensions.isVisible
import com.example.posts.fragments.BasePostsFragment
import com.example.posts.ui.item.PostItemUI
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.sdk25.coroutines.onClick

class PostsAdapter<T : BasePostModel>(val posts: ArrayList<T>, val owner: BasePostsFragment<T>) : RecyclerView.Adapter<PostsAdapter.PostsViewHolder>() {

    private val postUI = PostItemUI(owner.hasActions)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostsViewHolder {
        return PostsViewHolder(postUI.createView(AnkoContext.create(parent.context)))
    }

    override fun getItemCount() = posts.size + 1

    override fun onBindViewHolder(holder: PostsViewHolder, position: Int) {
        // dirty workaround to animate item remove
        if (position == posts.size) {
            holder.root.visibility = View.INVISIBLE
            return
        } else {
            holder.root.isVisible = true
        }
        val postEntity = posts[position]
        holder.bind(postEntity)
        holder.actionButton.onClick {
            owner.itemActionClicked(holder.adapterPosition, holder.actionButton)
        }
    }

    // Anko and viewHolder  ¯\_(ツ)_/¯
    class PostsViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val title: TextView = view.findViewById(R.id.post_title)
        private val body: TextView = view.findViewById(R.id.post_body)
        val root: View = view.findViewById(R.id.post_root)
        val actionButton: ImageButton = view.findViewById(R.id.post_actions)

        fun bind(postEntity: BasePostModel) {
            title.text = postEntity.title
            body.text = postEntity.body
        }
    }
}