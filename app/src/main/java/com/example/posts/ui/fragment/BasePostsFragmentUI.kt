package com.example.posts.ui.fragment

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.widget.ProgressBar
import com.example.posts.adapters.PostsAdapter
import com.example.posts.data.entities.BasePostModel
import com.example.posts.extensions.isVisible
import com.example.posts.fragments.BasePostsFragment
import org.jetbrains.anko.AnkoComponent

abstract class BasePostsFragmentUI<T : BasePostModel>(
        val owner: BasePostsFragment<T>) : AnkoComponent<Context> {
    protected abstract var progress: ProgressBar
    abstract var recycler: RecyclerView
    protected abstract val hasPopup: Boolean

    var postsAdapter: PostsAdapter<T>? = null

    fun onDataLoaded(posts: ArrayList<T>) {
        hideProgress()
        owner.posts = posts
        postsAdapter = PostsAdapter(owner.posts, owner)
        recycler.adapter = postsAdapter
    }

    private fun hideProgress() {
        progress.isVisible = false
    }

    fun onDataLoadingFailed() {
        hideProgress()
    }
}