package com.example.posts.data.sources

import com.example.posts.data.entities.InternetPostEntity
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

object ApiWrapper {
    private const val baseUrl = "https://jsonplaceholder.typicode.com"
    private val service: Service = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .client(OkHttpClient())
            .build()
            .create(Service::class.java)

    interface Service {
        @GET("posts")
        fun getPostEntity(): Call<ArrayList<InternetPostEntity>>
    }

    @Throws(Throwable::class)
    fun getAllPosts(onSuccess: (ArrayList<InternetPostEntity>) -> Unit, failure: (t: Throwable) -> Unit) {
        service.getPostEntity().enqueue(object : Callback<ArrayList<InternetPostEntity>> {
            override fun onFailure(call: Call<ArrayList<InternetPostEntity>>?, t: Throwable?) {
                failure(t ?: Throwable("Unknown error"))
            }

            override fun onResponse(call: Call<ArrayList<InternetPostEntity>>?, response: Response<ArrayList<InternetPostEntity>>?) {
                onSuccess(response?.body() ?: throw Throwable("Unknown error"))
            }
        })
    }
}