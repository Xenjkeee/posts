package com.example.posts.adapters

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.example.posts.fragments.BasePostsFragment

class MainPagerAdapter(fragmentManager: FragmentManager,
                       private val pageTitles: ArrayList<String>,
                       private val fragments: ArrayList<BasePostsFragment<*>>) : FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int) = fragments[position]
    override fun getCount() = pageTitles.size
    override fun getPageTitle(position: Int) = pageTitles[position]

}