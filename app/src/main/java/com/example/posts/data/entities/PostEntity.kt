package com.example.posts.data.entities

import com.example.posts.data.entities.BasePostModel.Companion.ID
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.Index
import io.realm.annotations.PrimaryKey

open class PostEntity : RealmObject(), BasePostModel {


    @PrimaryKey
    @Index
    @SerializedName(ID)
    @Expose
    var id: Int? = null

    @SerializedName("title")
    @Expose
    override var title: String? = null

    @SerializedName("body")
    @Expose
    override var body: String? = null

}


