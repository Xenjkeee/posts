package com.example.posts.data.sources

import com.example.posts.data.entities.BasePostModel
import com.example.posts.data.entities.PostEntity
import io.realm.Realm
import io.realm.RealmObject

object DataBaseWrapper {

    fun putPostMyPost(title: String, body: String): PostEntity {

        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        val postEntity = realm.createObject(PostEntity::class.java, getNextKey(realm)).apply {
            this.title = title
            this.body = body
        }
        realm.commitTransaction()
        return postEntity
    }

    fun removePost(postEntity: PostEntity) {
        Realm.getDefaultInstance().executeTransaction {
            it.where(PostEntity::class.java).equalTo(BasePostModel.ID, postEntity.id).findAll().deleteAllFromRealm()
        }
    }

    fun <T : RealmObject> putPosts(postEntities: Iterable<T>) {
        Realm.getDefaultInstance().executeTransaction {
            it.copyToRealmOrUpdate(postEntities)
        }
    }

    inline fun <reified T : RealmObject> getAllPosts(clazz: Class<T>): ArrayList<T> {
        return ArrayList(Realm.getDefaultInstance().where(clazz).findAll())
    }

    private fun getNextKey(realm: Realm): Int {
        return try {
            val number = realm.where(PostEntity::class.java).max("id")
            if (number != null) {
                number.toInt() + 1
            } else {
                0
            }
        } catch (e: ArrayIndexOutOfBoundsException) {
            0
        }

    }

    fun editPost(post: PostEntity, title: String, body: String) {
        Realm.getDefaultInstance().executeTransaction {
            post.title = title
            post.body = body
        }
    }
}