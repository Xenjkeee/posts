package com.example.posts.ui.fragment

import android.content.Context
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.LinearLayout
import android.widget.ProgressBar
import com.example.posts.R
import com.example.posts.data.entities.PostEntity
import com.example.posts.fragments.LocalPostsFragment
import org.jetbrains.anko.*
import org.jetbrains.anko.design.floatingActionButton
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.sdk25.coroutines.onClick
import kotlin.properties.Delegates


class LocalPostsFragmentUI(localPostsFragment: LocalPostsFragment) : BasePostsFragmentUI<PostEntity>(localPostsFragment) {
    override var progress: ProgressBar by Delegates.notNull()

    override var recycler: RecyclerView by Delegates.notNull()

    private lateinit var addPostButton: FloatingActionButton
    override val hasPopup: Boolean = true

    override fun createView(ui: AnkoContext<Context>) = with(ui) {
        relativeLayout {

            recycler = recyclerView {
                layoutManager = LinearLayoutManager(owner, LinearLayout.VERTICAL, false)
            }

            progress = progressBar().lparams {
                centerInParent()
            }

            addPostButton = floatingActionButton {
                imageResource = R.drawable.add_icon
                onClick {
                    (this@LocalPostsFragmentUI.owner as LocalPostsFragment).showEditDialog()
                }
            }.lparams {
                alignParentBottom()
                alignParentRight()
                rightMargin = dip(20)
                bottomMargin = dip(20)
            }
        }

    }

}

