package com.example.posts.data.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.Index
import io.realm.annotations.PrimaryKey

open class InternetPostEntity : RealmObject(), BasePostModel {

    @SerializedName("userId")
    @Expose
    var userId: Int? = null


    @PrimaryKey
    @Index
    @SerializedName(BasePostModel.ID)
    @Expose
    var id: Int? = null

    @SerializedName("title")
    @Expose
    override var title: String? = null

    @SerializedName("body")
    @Expose
    override var body: String? = null

}