package com.example.posts.fragments

import android.util.Log
import com.example.posts.data.entities.InternetPostEntity
import com.example.posts.data.sources.ApiWrapper
import com.example.posts.data.sources.DataBaseWrapper
import com.example.posts.ui.fragment.BasePostsFragmentUI
import com.example.posts.ui.fragment.InternetPostsFragmentUI

class InternetPostsFragment : BasePostsFragment<InternetPostEntity>() {
    override val ui: BasePostsFragmentUI<InternetPostEntity> = InternetPostsFragmentUI(this)
    override val hasActions: Boolean = false

    override fun loadData() {
        try {
            ApiWrapper.getAllPosts({
                ui.onDataLoaded(it)
                DataBaseWrapper.putPosts(it)
            }) {
                onFailure(it)
            }
        } catch (t: Throwable) {
            onFailure(t)
        }

    }

    private fun onFailure(t: Throwable) {
        ui.onDataLoadingFailed()
        Log.e("Api", t.message)
        ui.onDataLoaded(DataBaseWrapper.getAllPosts(InternetPostEntity::class.java))
    }
}

